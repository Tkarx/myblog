from django.urls import path, re_path
from django.conf.urls import url

from .views import index
from .views import detail
from .views import contact
from .views import tag
from .views import search
# from .views import test404
# from .views import test500


app_name = 'home'
urlpatterns = [
    # hostname/home
    path('',index ,name='index'),
    
    url(r'^results/(?P<name>.+)/$', search, name='search'),

    # hostname.ex/detail/ 
    path('post/<slug:slug>/', detail , name = 'detail'),
    # hostname.ex/tag/linux/ 
    path('tag/<slug:slug>/', tag , name = 'tag'),
    # hostname.ex/contact
    path('us/contact', contact, name='contact'),

    # Error Handeling
    # path('404/', test404),
    # path('500/', test500),
]

