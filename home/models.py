from django.db import models
from django.utils import timezone
from ckeditor.fields import RichTextField
import os
from django.utils.html import format_html

class Tag(models.Model):
    name = models.CharField(max_length = 200)
    slug = models.SlugField(max_length=100)
    def __str__(self):
        return self.name

class Post(models.Model):
    title = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)
    pic = models.ImageField(upload_to="cover/")
    body = RichTextField()
    published = models.DateTimeField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    tag = models.ManyToManyField(Tag)

    STATUS_CHOICES = (
        ('deaft', 'Draft'),
        ('published' , 'Published')
    )
    status = models.CharField(max_length=60 , choices=STATUS_CHOICES , default= 'draft')

    REC_CHOICES = (
        ('yes', 'Yes'),
        ('no', 'No'),
    )
    recommend = models.CharField(max_length=60 , choices=REC_CHOICES , default= 'no')

    def __str__(self):
        return self.title
    
    def thumbnail(self):
        return format_html("<img src='{}' width='80' height='50' style='border-radius: 5px;'>".format(self.pic.url))


