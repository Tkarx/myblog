# Generated by Django 3.0.5 on 2020-06-19 20:28

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('slug', models.SlugField(max_length=100)),
                ('pic', models.ImageField(upload_to='media/static/img')),
                ('body', models.TextField()),
                ('published', models.DateTimeField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('status', models.CharField(choices=[('deaft', 'Draft'), ('published', 'Published')], default='draft', max_length=60)),
            ],
        ),
    ]
