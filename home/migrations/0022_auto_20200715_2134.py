# Generated by Django 3.0.7 on 2020-07-15 17:04

from django.db import migrations
import froala_editor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0021_post_content'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='post',
            name='content',
        ),
        migrations.AlterField(
            model_name='post',
            name='body',
            field=froala_editor.fields.FroalaField(),
        ),
    ]
