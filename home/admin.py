from django.contrib import admin

from .models import Post, Tag
# Register your models here.


# Change Admin header
admin.site.site_header = "Welcome Omid :)"


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('name',)
    prepopulated_fields = {'slug':("name",)}

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'thumbnail', 'published', 'display_tag', 'status')
    list_filter = ('published', 'tag', 'status')
    search_fields = ('title', 'body')
    prepopulated_fields = {'slug':("title",)}
    ordering = ['status', '-published']
    # fields 

    def display_tag(self, obj):
        return ', '.join([tag.name for tag in obj.tag.all()[:3]])

    display_tag.short_description = 'Tag'

