from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import Q
from django.shortcuts import (
    HttpResponse, get_list_or_404, get_object_or_404, render)


from .models import Post, Tag

# Create your views here.

def tags():
    return Tag.objects.all()

def paginator(request, data, num):
    
    paginator = Paginator(data, num)
    page = request.GET.get('page')
    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        items = paginator.page(1)
    except EmptyPage:
        items = paginator.page(paginator.num_pages)
    index = items.number - 1
    max_index = len(paginator.page_range)
    start_index = index - 5 if index >= 5 else 0
    end_index = index + 5 if index <= max_index - 5 else max_index
    page_range = paginator.page_range[start_index:end_index]

    return items, page_range

def recommend():
    return Post.objects.filter(recommend="yes").order_by('-published')[:5]

def index(request):
    posts = Post.objects.filter(status='published').order_by('-published')

    pages = paginator(request, posts, 3)
    context = {
        'items' : pages[0],
        'tags' : tags,
        'recommend' : recommend,
    }
    return render(request, 'home/index.html', context)


def detail(request, slug):
    post = get_object_or_404(Post, slug = slug)

    context = {
        'post' : post,
        'tags' : tags,
        'recommend' : recommend,
    }
    return render(request, 'home/detail.html',context)

def contact(request):
    context = {
        'tags' : tags,
        'recommend' : recommend,
    }
    return render(request, 'home/contact.html', context)

def tag(request, slug):
    posts = Post.objects.filter(tag__slug = slug, status='published')
    pages = paginator(request, posts, 5)

    tag = Tag.objects.filter(slug=slug)

    context = {
        'items' :pages[0],
        'tag' : tag,
        'tags' : tags,
        'recommend' : recommend,
    }
    return render(request, 'home/taglist.html', context)

def search(request,name):

    query = request.GET.get('q')
    posts = Post.objects.filter(Q (status="published") , Q(title__icontains=query) | Q(body__icontains = query) ).order_by("-published")
    context = {
        'items' : posts,
        'tags' : tags,
        'query' : query,
        'recommend' : recommend,
    }

    return render(request, 'home/search.html', context)












# Error Handeling
def handler404(request, exception):
    return render(request , "error/404.html", status = 404)

# def test404(request):
#     return render(request , "error/404.html")

def handler500(request):
    return render(request , "error/500.html", status = 500)

# def test500(request):
#     return render(request , "error/500.html")
